extends Sprite3D

var tween: Tween
var move_distance = 100  # Entfernung, um sich horizontal zu bewegen
var move_duration = 1.0  # Dauer einer Bewegung in Sekunden

var org_pos = position

func _ready():
	animate()
	
func animate():
	tween = create_tween()
	
	# Use set_delay() to delay the execution of a tween:
	tween.tween_property(self, "position:x", org_pos.x - randi_range(-2, 2), 30.0)
	tween.chain().tween_property(self, "color", org_pos.x, 30.0)
	tween.tween_interval(1)
	tween.tween_callback(animate)
