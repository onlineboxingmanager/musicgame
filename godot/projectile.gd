class_name Bat
extends Area3D

@export var speed: float = 2.5
@export var velocity: float = 0.5
@export var is_projectile: bool = false
@export var damage: int = 10
@export var is_fly_around = false
var direction = Vector3(0, 0, 1)

var start_pos : Vector3
var target_pos : Vector3
var end_pos : Vector3

func _ready():
	start_pos = global_position
	target_pos = start_pos + direction
	end_pos = start_pos + Vector3(0, 0, 10)

func _process(delta):
	if is_projectile:
	 # Bewegung entlang der lokalen Z-Achse des Objekts
		var motion = Vector3(0, 0, speed * delta)
		translate(motion)  # Bewegung entlang der lokalen Z-Achse des Objekts
	
	#print("global_position", global_position)
	if( global_position.z > end_pos.z ):
		print("clear bat")
		queue_free()

func _on_body_entered(body):
	if body.is_in_group("Player"):
		body.take_damage(self)
		queue_free()
