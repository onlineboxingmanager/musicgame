extends Node

var current_scene
signal scene_changed

func _process(delta):
	# Feels dirty
	if current_scene != get_tree().get_current_scene():
		emit_signal("scene_changed")
		current_scene = get_tree().get_current_scene()

func switch_to(scene_file_path):
	# 1. Trigger "leaving level"-transition and await its end

	# 2. Switch scene
	var scene = ResourceLoader.load_threaded_get(scene_file_path) # This was loaded before somewhere else
	get_tree().change_scene_to_packed(scene)
	
	# 3. Await switch
	await scene_changed
	
	# 4. Spawn player and trigger "entering level"-transition
