extends Sprite3D

var rotation_speed = 0.01  # Geschwindigkeit der Rotation

func _process(delta):
	# Ändere die Rotation des Sprites um die Y-Achse basierend auf der Zeit und der Rotationsgeschwindigkeit
	rotate_z(rotation_speed * delta)
