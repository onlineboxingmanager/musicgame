class_name VampireScene
extends Node3D

var eventlist = []
var bat_scene: Resource

@onready var vampire: Vampire = %Vampire

# Called when the node enters the scene tree for the first time.
func _ready():
	$Player.doIdle()
	$Player.setHealthbar($"SubViewportContainer/SubViewport/MarginContainer/Lifebar-Player/Healthbar-Player")
	bat_scene = load("res://bat.tscn")
	
	start_scene()

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass
	
func start_scene():
	var file = FileAccess.open("res://Beat.txt", FileAccess.READ)
	var text = file.get_as_text()
	file.close()

	var lines = text.split("\r\n")
	for line in lines:
		var values = line.split("\t")
		eventlist.append(values)

		var position = 0
		if len(values) >= 3 and len(values[2]) > 1:
			position = int(values[2].substr(1))

		var type = "r"
		if len(values) >= 3:
			type = values[2].substr(0, 1)

		var delay = float(values[0])
		
		_process_delayed_event(delay, type, position)

	# Annahme: Sound-Node existiert als Kind des Hauptknotens
	$AudioStreamPlayer2D.play()
	
func _process_delayed_event(delay: float, type: String, position: int):
	await get_tree().create_timer(delay).timeout
		
	if type == "B":
		do_beat()
	else:
		add_projectile(type, position)

func do_beat():
	vampire.doBeat()
	
	# Implementiere die Aktion für einen Beat
	%Camera3D._camera_shake()

func add_projectile(type: String, position: int):
	# Implementiere die Aktion zum Hinzufügen eines Projektils
	
	var bat: Area3D = bat_scene.instantiate()
	get_tree().get_root().add_child(bat)
	
	bat.is_projectile = true
		
	var pos = position - 2;
	if( position == 0 ):
		pos = 0;
		
	bat.global_position.z = 0
	bat.global_position.x = pos - 1
	bat.global_position.y = 0
	
	vampire.doShot()

func game_over():
	$AudioStreamPlayer2D.stop()
	
	vampire.animations.play('walk') #TODO: hier animation für Sieges-Pose/Jubel oder what evera
	
	# wait 5 sec to show game over
	await get_tree().create_timer(5).timeout
		
	get_tree().change_scene_to_file ("res://game_over.tscn")
