class_name Player
extends CharacterBody3D

@export var life: int = 100

const SPEED = 5.0
const JUMP_VELOCITY = 4.5

# Get the gravity from the project settings to be synced with RigidBody nodes.
var gravity = ProjectSettings.get_setting("physics/3d/default_gravity")
var animations: AnimatedSprite3D

var max_life: int = life
var is_paralized: bool = false

var lifebar: Lifebar

func _ready():
	animations = get_node("joe")
	
#	var lifebar: Lifebar = %"Lifebar-Player"
#	print("life", lifebar)

func _input(event):
	if event is InputEventKey && event.is_pressed():
		
		if !is_paralized:
			if event.as_text_physical_keycode() == "D" || event.as_text_physical_keycode() == "Right":
				position.x += 1
			if event.as_text_physical_keycode() == "A" || event.as_text_physical_keycode() == "Left":
				position.x -= 1
				
			if position.x > 2:
				position.x = 2
			if position.x < -2:
				position.x = -2
				
			doIdle()

func _physics_process(delta):
	# Add the gravity.
	if not is_on_floor():
		velocity.y -= gravity * delta

	# Handle jump.
	if Input.is_action_just_pressed("jump") and is_on_floor():
		velocity.y = JUMP_VELOCITY
		animations.play("CAST_UP")

	move_and_slide()

func doHurt():
	animations.play("HURT")
	
func doDead():	
	is_paralized = true
	animations.play("HURT_DOWN")
	$"..".game_over()
		
func take_damage(source):
	# hp balken hier
	#....
	
	life -= source.damage
	lifebar._set_health(life)
	if( life <= 0 && !is_paralized ):
		doDead()
		return
	
	if( life <= 0 && is_paralized ):
		return
	
	doHurt()
	$AudioStreamPlayer2D.play()
	
	#animations.material_override.set_shader_parameter("active",true)
	#await get_tree().create_timer(0.2).timeout
	#animations.material_override.set_shader_parameter("active",false)

func damage(dmg):
	pass

func doIdle():
	var animations: AnimatedSprite3D = get_node("joe")
	animations.play("THRUST_UP")

func _on_joe_animation_finished():
	if animations.get_animation() == "CAST_UP":
		doIdle()

func setHealthbar(item: Lifebar):
	lifebar = item
	item.init_health(max_life)
	item._set_health(life)
