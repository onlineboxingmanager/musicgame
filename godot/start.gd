class_name StartScene
extends Control

var game_scene = preload("res://game.tscn").instantiate()

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func _input(event):
	if event is InputEventKey:
		if event.pressed:
			press_start()

func press_start():
	#LevelSwitcher.switch_to("res://game.tscn")
	#get_tree().unload_current_scene()
	get_tree().change_scene_to_file ("res://game.tscn")
