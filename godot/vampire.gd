class_name Vampire
extends Node3D

@onready var animations: AnimationPlayer = $AnimationPlayer

func doIdle():
	animations.play("idle")

func doBeat():
	animations.play("shot")

func doShot():
	#animations.stop()
	animations.play("shot")
	
func doDead():
	pass
	
func doHit():
	pass
