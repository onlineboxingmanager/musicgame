/**
 * This class provides to functionality to animate sprite sheets.
 */
export class SpriteTileset {

	/**
	 *
	 * @param spriteTexture A sprite sheet with sprite tiles
	 * @param tilesHoriz Horizontal number of tiles
	 * @param tilesVert Vertical number of tiles
	 */
	constructor(spriteTexture, tilesHoriz, tilesVert, tileSize, Game) {
		this.tilesHoriz = tilesHoriz;
		this.tilesVert = tilesVert;

		this.tileHeight = tileSize;
		this.tileWidth = tileSize;
		this.animation_time = 0;

		this.game = Game;
		this.engine = Game.engine;
		this.clock = new this.engine.Clock();
		this.elapsedTime = 0;

		this.map = new this.engine.TextureLoader().load(spriteTexture);
		this.map.magFilter = this.engine.NearestFilter;   // sharp pixel sprite
		this.map.repeat.set(1 / tilesHoriz, 1 / tilesVert);

		const material = new this.engine.SpriteMaterial({
			map: this.map,
			depthWrite: false
		});

		this.sprite = new this.engine.Sprite(material);
		//console.log(this.sprite.geometry.computeBoundingBox());
		this.sprite.geometry.computeBoundingBox(); // create 3D box (hitboxes for our chars)
		//console.log(this.sprite.geometry);

		this.position = this.sprite.position;
	}

	setAnimation(row, start, length, duration, callback)
	{
		let playSpriteIndices = [];

		for( let i = 0; i < length; i++)
		{
			playSpriteIndices.push( ( row * this.tilesHoriz ) + start + i  );
		}

		this.animation(playSpriteIndices, duration, callback);
	}

	loop(playSpriteIndices, totalDuration)
	{
		this.is_loop = true;
		this.callback = null;
		this.playSpriteIndices = playSpriteIndices;
		this.runningTileArrayIndex = 0;
		this.currentTile = playSpriteIndices[this.runningTileArrayIndex];
		this.maxDisplayTime = totalDuration / this.playSpriteIndices.length;
		this.elapsedTime = this.maxDisplayTime; // force to play new animation
	}

	animation(playSpriteIndices, totalDuration, callback)
	{
		this.is_loop = false;
		this.callback = callback;
		this.playSpriteIndices = playSpriteIndices;
		this.runningTileArrayIndex = 0;
		this.currentTile = playSpriteIndices[this.runningTileArrayIndex];
		this.maxDisplayTime = totalDuration / this.playSpriteIndices.length;
		this.elapsedTime = this.maxDisplayTime; // force to play new animation
	}

	setPosition(x, y, z) {
		this.sprite.position.x = x;
		this.sprite.position.y = y;
		this.sprite.position.z = z;
	}

	addPosition(x, y, z) {
		this.sprite.position.x += x;
		this.sprite.position.y += y;
		this.sprite.position.z += z;
	}

	getPosition() {
		return this.sprite.position;
	}

	update(delta) {
		this.elapsedTime += delta;

		if (this.maxDisplayTime > 0 && this.elapsedTime >= this.maxDisplayTime)
		{
			this.elapsedTime = 0;
			this.runningTileArrayIndex = (this.runningTileArrayIndex + 1) % this.playSpriteIndices.length;
			this.currentTile = this.playSpriteIndices[this.runningTileArrayIndex];

			const offsetX = (this.currentTile % this.tilesHoriz) / this.tilesHoriz;
			const offsetY = (this.tilesVert - Math.floor(this.currentTile / this.tilesHoriz) - 1) / this.tilesVert;

			this.map.offset.x = offsetX;
			this.map.offset.y = offsetY;

			// end of loop
			if( this.is_loop == false && this.callback && this.runningTileArrayIndex == this.playSpriteIndices.length - 1 )
			{
				this.callback();
			}
		}
	}

	idle()
	{

	}

	walk()
	{

	}
}