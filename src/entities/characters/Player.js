import {SpriteTileset} from "../../SpriteTileset.js";

export class Player extends SpriteTileset
{
	constructor(Game)
	{
		super('/src/sprites/player.png', 13, 21, 64, Game);

		this.idle();
	}

	moveRight()
	{
		console.log("right");
		if( this.sprite.position.z > -2 )
			this.sprite.position.z -= 1;

		this.idle();
	}

	moveLeft()
	{
		if( this.sprite.position.z < 2 )
			this.sprite.position.z += 1;

		this.idle();
	}

	moveUp()
	{
		if( this.sprite.position.y < 2 )
			this.sprite.position.y += 1;

		this.is_jump = true;
		this.jump();
	}

	update(delta)
	{
		this.animation_time += delta;
		super.update(delta);
	}

	idle()
	{
		this.setAnimation(4, 1, 7, 1.5);
	}

	jump()
	{
		this.setAnimation(0, 3, 4, 1.0, () => { this.idle() });
	}

	shot()
	{
		this.setAnimation(15, 0, 6, 2.5);
	}

	hit()
	{
		this.setAnimation(20, 0, 6, 1, () => { this.idle() });
	}

}