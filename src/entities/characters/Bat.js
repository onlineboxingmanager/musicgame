import {SpriteTileset} from "../../SpriteTileset.js";

export class Bat extends SpriteTileset
{
	constructor(Game)
	{
		super('/src/sprites/bat.png', 2, 1, 34, Game);
		this.is_fly = false;
		this.fly_around_position = {x: 0, y: 0, z: 0};
	}

	update(delta)
	{
		this.animation_time += delta;

		// ---------------- UPDATING MAIN OBJECTS POSITION  ----------------
		if( this.is_fly )
			this.setPosition( this.fly_around_position.x + Math.sin(this.animation_time), this.fly_around_position.y + Math.cos(this.animation_time ) / 2 + 1, this.fly_around_position.z + Math.cos(this.animation_time))

		super.update(delta);
	}

	idle() {
		this.loop([0, 1], 1);
	}

	walk() {
		this.loop([0, 1], 1);
	}

	fly()
	{
		this.walk();
		this.is_fly = true;
	}

	flyAround(position)
	{
		this.fly_around_position = position;
		this.fly();
	}

}