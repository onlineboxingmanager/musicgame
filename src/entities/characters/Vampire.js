import {SpriteTileset} from "../../SpriteTileset.js";

export class Vampire extends SpriteTileset
{
	constructor(Game)
	{
		super('/src/sprites/vampire.png', 8, 2, 64, Game);

		this.movement = null;
		this.velocity = 5;
		this.sound = null;

		this.timer = null;

		this.walkFromOutside();
	}

	update(delta)
	{
		this.animation_time += delta;
		this.doMovement(delta)

		let self = this;

		if( this.sound )
		{
			//console.log( this.sound.context.currentTime );
			if( !this.timer )
			{
				setTimeout(function()
				{
					self.screenPositionNear();
				}, 45 * 1000);
			}
		}

		super.update(delta);
	}

	doMovement(delta)
	{
		if(this.movement )
			this.movement(delta);
	}

	walkFromOutside()
	{
		this.idle();
		this.screenPositionFar();

		this.movement = function(delta)
		{
			if( this.sprite.position.x >= -80 )
				return this.screenPositionNear();

			this.sprite.position.x += delta * this.velocity;
		}
	}

	screenPositionNear()
	{
		this.sprite.scale.set(5, 5, 5)
		this.setPosition(-8, 1, 0);
	}

	screenPositionFar()
	{
		this.sprite.scale.set(50, 50, 50)
		this.setPosition(-500, 1, 0);
	}

	idle() {
		this.loop([8, 9], 1);
	}

	walk() {
		this.loop([0, 1, 2, 3, 4], 2.5);
	}

	shot(callback)
	{
		this.animation([8, 9, 10, 11, 12, 13], 2.5, ()=>{ this.idle() });
	}

}