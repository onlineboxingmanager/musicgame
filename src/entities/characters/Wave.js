import {SpriteTileset} from "../../SpriteTileset.js";

export class Wave extends SpriteTileset
{
	constructor(Game)
	{
		super('/src/sprites/wave.png', 3, 1, 106, Game);
		this.is_fly = false;
	}

	update(delta)
	{
		this.animation_time += delta;

		super.update(delta);
	}

	idle() {
		this.loop([0, 1, 2], 0.5);
	}

	walk() {
		this.loop([0, 1, 2], 0.5);
	}

	fly()
	{
		this.walk();
		this.is_fly = true;
	}


}