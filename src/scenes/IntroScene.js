import {BaseScene} from "./BaseScene.js";
import {FpsCounter} from "../FpsCounter.js";
import {FightScene} from "./FightScene.js";

export class IntroScene extends BaseScene
{
	constructor(Game)
	{
		super(Game);

		this.on_enter_command = function()
		{
			Game.nextScene();
		};
	}

	start()
	{
		this.init();
		this.is_started = true;

		const THREE = this.engine;
		const textureLoader = new THREE.TextureLoader();
		let self = this;

		textureLoader.load('/src/sprites/intro.png', texture =>
		{
			const material = new self.engine.MeshLambertMaterial({
				//color: 0xff0000,
				map: texture,
				transparent: true,
				opacity: 1,
				side: THREE.DoubleSide,
				depthWrite: false,
			});
			//material.map.minFilter = self.engine.LinearFilter;
			//material.map.rotation = Math.PI / 2;

			const geometry = new self.game.engine.PlaneGeometry( 10, 5 );
			let intro = new self.game.engine.Mesh( geometry, material );
			intro.position.x = 0;
			intro.position.z = 0
			intro.rotation.y = ( Math.PI / 2 )
			intro.velocity_ = 0.2;
			intro.updateMatrix();
			self.scene.add(intro);
		});
	}

}