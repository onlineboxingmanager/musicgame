import {BaseScene} from "./BaseScene.js";
import {SpriteTileset} from "../SpriteTileset.js";
import {Bat} from "../entities/characters/Bat.js";
import {Wave} from "../entities/characters/Wave.js";
import {Vampire} from "../entities/characters/Vampire.js";
import {Player} from "../entities/characters/Player.js";
import * as THREE from '/src/three.module.js';

export class FightScene extends BaseScene
{
	constructor(Game)
	{
		super(Game);

		this.projectiles = [];
		this.group = [];
		this.last_shot_position = null;
		this.beatTimer = null;
		this.on_enter_command = function (){ };
	}

	start()
	{
		this.init();

		let self = this;
		const THREE = this.engine;
		console.log("fight start");

		this.addBackground();

		// grid helper
		//this.scene.add( new THREE.GridHelper() );
		// axis helper debug
		//this.scene.add(new Game.engine.AxesHelper(5))

		// enemy
		this.enemy = new Vampire(this.game);
		this.scene.add( this.enemy.sprite );
		this.entities.push(this.enemy);

		// player
		this.player = new Player(this.game);
		this.player.setPosition(0, 0, 0);
		this.scene.add( this.player.sprite );
		this.entities.push(this.player);
		this.game.player = this.player;

		this.bat = new Bat(this.game);
		this.bat.sprite.scale.set(2, 2, 2)
		this.bat.flyAround(this.enemy.sprite.position);
		this.scene.add(this.bat.sprite);
		this.entities.push(this.bat);

		this.camera_movement = 0.00;

		// animation + start sound
		setTimeout(function()
		{
			self.playAudio();
		}, 1000);
	}

	addBackground()
	{
		const THREE = this.engine;
		const textureLoader = new THREE.TextureLoader();
		let self = this;

		textureLoader.load('/src/sprites/ground.png', texture =>
		{
			const material = new self.engine.MeshLambertMaterial({
				color: 0xff0000,
				map: texture,
				transparent: true,
				opacity: 0.7,
				side: THREE.DoubleSide,
				depthWrite: false,
				//needsUpdate: false,
				depthTest: false,
				blending: THREE.AdditiveBlending,
			});
			material.map.minFilter = self.engine.LinearFilter;
			material.map.rotation = Math.PI / 2;

			const geometry = new self.game.engine.PlaneGeometry( 10, 5 );
			self.ground = new self.game.engine.Mesh( geometry, material );
			self.ground.position.x = ( 10 / 2 ) * -1 // rot
			self.ground.position.y = -0.5 // grün
			self.ground.position.z = 0 // blau
			self.ground.rotation.x = Math.PI / 2;

			self.ground.matrixAutoUpdate = false;
			self.ground.updateMatrix();

			self.scene.add(self.ground);
			self.group.push(self.ground);
		});

		const textGeometry = new THREE.PlaneGeometry(64, 48);
		textureLoader.load('/src/sprites/moon.png', texture => {
			const textMaterial = new THREE.MeshLambertMaterial({
				blending: THREE.AdditiveBlending,
				color: 0xff0000,
				map: texture,
				opacity: 1,
				transparent: true,
				depthWrite: false,
				//depthTest: false,
				//blending: THREE.AdditiveBlending,
			});
			textMaterial.map.minFilter = THREE.LinearFilter;
			const text = new THREE.Mesh(textGeometry, textMaterial);

			text.position.x = -100;
			text.position.y = 10
			text.position.z = 20
			text.rotation.y = Math.PI / 2
			self.scene.add(text);

			self.background = text;
			self.background.velocity_ = 0.1;

			self.group.push(self.background);
		});

		this.clouds = [];
		textureLoader.load('/src/sprites/clouds.png', texture =>
		{
			const textMaterial = new THREE.MeshLambertMaterial({
				blending: THREE.AdditiveBlending,
				color: 0xffffff,
				map: texture,
				opacity: 1,
				transparent: true,
				depthWrite: false,
			});
			textMaterial.map.minFilter = THREE.LinearFilter;
			let text = new THREE.Mesh(textGeometry, textMaterial);
			text.position.x = -80;
			text.position.y = 10
			text.position.z = 30
			text.rotation.y = Math.PI / 2
			text.velocity_ = 0.2;
			self.scene.add(text);
			self.clouds.push(text);
			self.group.push(text);

			text = new THREE.Mesh(textGeometry, textMaterial);
			text.position.x = -70;
			text.position.y = 5
			text.position.z = -30
			text.rotation.y = Math.PI / 2
			text.velocity_ = 0.2;
			self.scene.add(text);
			self.clouds.push(text);
			self.group.push(text);
		});
	}

	playAudio()
	{
		let self = this;

		this.eventlist = {};

		// create an AudioListener and add it to the camera
		const listener = new this.engine.AudioListener();
		this.camera.add( listener );

		// create a global audio source
		const sound = new this.engine.Audio( listener );

		// load a sound and set it as the Audio object's buffer
		const audioLoader = new this.engine.AudioLoader();
		audioLoader.load( '/src/sounds/System_Crash.wav', function( buffer )
		{
			sound.setBuffer( buffer );
			sound.setLoop(false);
			sound.setVolume(0.85);

			self.sound = sound;
			self.enemy.sound = self.sound;

			let FileLoader = new self.engine.FileLoader();
			FileLoader.load( "/Beat.txt", function( text )
			{
				const lines = text.split("\r\n");
				lines.forEach(function(line, index)
				{
					let values = line.split("\t");

					self.eventlist[index] = values;

					let position = 0;
					if( values[2] && values[2].length > 1 )
					{
						position = parseInt(values[2].substring(1));
					}

					let type = "r";
					if( values[2] )
						type = values[2].substring(0, 1);

					// verbraucht viel speicher
					setTimeout(function()
					{
						// beat
						if( type == 'B' )
						{
							self.doBeat();
						}

						else
						{
							self.addProjectile(type, position);
						}

					}, values[0] * 1000);
				});

				self.sound.play();
			});

		});
	}

	doBeat()
	{
		let self = this;
		this.camera.position.y += 0.1;
		this.camera.position.x += -0.1;
		this.camera.position.z += this.randomIntFromInterval(-10,10) / 100;
		clearInterval(this.beatTimer)
		this.beatTimer = setTimeout(function()
		{
			self.camera.position.y = self.camera_org_position.y;
			self.camera.position.x = self.camera_org_position.x;
			self.camera.position.z = self.camera_org_position.z;
		}, 250)

		this.enemy.shot(function()
		{
			this.enemy.idle();
		});
	}

	addProjectile(type, position, velocity)
	{
		let shot = new Bat(this.game);
		shot.velocity = 0.5;

		if( type == 'w' )
		{
			shot = new Wave(this.game);
			shot.velocity = 0.25;
		}

		shot.sprite.position.z = 0;
		shot.sprite.position.x = -10;
		shot.sprite.position.y = 0;
		if( position == 0 || type == '' || type == 'r' )
		{
			while(1 == 1)
			{
				position = this.randomIntFromInterval(1,5);
				if( position != this.last_shot_position )
					break;
			}
		}

		shot.sprite.position.z = 3 - position;
		this.last_shot_position = shot.sprite.position.z;

		shot.walk();
		this.scene.add(shot.sprite);
		this.projectiles.push(shot);

		// enemy do shot animation
		this.enemy.shot();
	}

	animateShot(deltaTime)
	{
		let self = this;

		for( let i in this.projectiles )
		{
			let pos = this.projectiles[i].sprite.position;

			this.projectiles[i].update(deltaTime);

			this.projectiles[i].position.x += 0.2 * this.projectiles[i].velocity;

			// console.log("player x " + player.position.x);
			// console.log("player z " + player.position.z);
			// console.log("s x " + self.projectiles[i].position.x);
			// console.log("s z " + self.projectiles[i].position.z);
			if( self.projectiles[i].position.x >= this.player.position.x && pos.x <= ( this.player.position.x + 1) && pos.z == this.player.position.z && pos.y == this.player.position.y )
			{
				console.warn('HIT !!');

				self.player.hit();
			}

			if( pos.x > 10 )
			{
				this.scene.remove(self.projectiles[i].sprite);
				delete self.projectiles[i];

				//self.addProjectile();
			}
		}
	}

	update(deltaTime)
	{
		this.player.position.y -= 0.1
		if( this.player.position.y < 0 )
			this.player.position.y = 0;

		this.entities.forEach(e => e.update(deltaTime));

		this.updateBackground(deltaTime);
		this.animateShot(deltaTime);
		this.updateClouds(deltaTime);

		//this.camera.position.x = Math.sin( deltaTime ) * 2;
		//this.camera.position.z = Math.cos( deltaTime ) * 2;
		//this.camera.lookAt(-4,0,0);

		super.update(deltaTime);
	}

	updateClouds(delta)
	{
		let smokeParticlesLength = this.clouds.length;
		while(smokeParticlesLength--)
		{
			this.clouds[smokeParticlesLength].position.z += delta * this.clouds[smokeParticlesLength].velocity_;

			if( this.clouds[smokeParticlesLength].position.z > 30 )
			{
				this.clouds[smokeParticlesLength].velocity_ = -0.2;
			}

			if( this.clouds[smokeParticlesLength].position.z < -30 )
			{
				this.clouds[smokeParticlesLength].velocity_ = 0.2;
			}
		}
	}

	updateBackground(delta)
	{
		if( !this.background )
			return false;

		this.background.rotation.z += delta * 0.01;
		this.background.position.z += delta * this.background.velocity_;

		if( this.background.position.z > 10 )
		{
			this.background.velocity_ = -0.1;
		}

		if( this.background.position.z < -10 )
		{
			this.background.velocity_ = 0.1;
		}
	}

}