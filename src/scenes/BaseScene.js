import OrbitControls from 'https://cdn.skypack.dev/threejs-orbit-controls';

export class BaseScene
{
	constructor(Game)
	{
		let self = this;

		this.on_enter_command = function (){};
		this.entities = [];
		this.game = Game;
		this.delta = 0;

		this.engine = Game.engine;
		this.sound = null;
		this.is_started = false;
		this.is_pause = false;

		// new scene instance
		this.scene = new Game.engine.Scene();

		this.setRenderer();

		// add camera for every scene
		//this.camera = new this.game.engine.PerspectiveCamera( 45, this.game.width / this.game.height, 1, 1000 );

		this.stats = Stats();
		document.body.appendChild(this.stats.domElement)

		//this.controls = new OrbitControls(this.camera, this.renderer.domElement);
		//this.controls.target.set(0, 5, 0);
		//this.controls.update();

		// bindings
		window.addEventListener( 'resize', function ()
		{
			self.resize();
		});

		this.resize();
	}

	setRenderer()
	{
		this.scene.fog = new this.engine.FogExp2(0x11111f, 0.002);

		this.renderer = new this.engine.WebGLRenderer({ antialias: true } );
		this.renderer.setSize( window.innerWidth, window.innerHeight );
		this.renderer.setClearColor(this.scene.fog.color);
		//this.renderer.clearColor(0x000000);

		//this.renderer.setPixelRatio( window.devicePixelRatio ); // TODO: Use player.setPixelRatio()
		//this.renderer.outputEncoding = this.engine.sRGBEncoding;

		//this.renderer.xr.enabled = false;
		//this.renderer.shadowMap.enabled = true;
		//this.renderer.shadowMap.type = 1;
		//this.renderer.toneMapping = 0;
		//this.renderer.toneMappingExposure = 1;
		//this.renderer.physicallyCorrectLights = false;

		document.body.appendChild( this.renderer.domElement );
	}

	init()
	{
		console.log(":: INIT");

		this.setCamera( new this.game.engine.PerspectiveCamera( 75, window.innerWidth / window.innerHeight, 1, 1000 ) );

		this.camera.position.y = 3;
		this.camera.position.x = 3;
		//this.camera.rotation.x = Math.PI / 2;
		//this.camera.zoom = 1;
		this.camera.lookAt( -4, 0, 0 );
		//this.camera.focus = 1;
		//this.camera.updateProjectionMatrix();
		this.game.camera = this.camera;

		this.camera_org_position = this.camera.position.clone();

		this.ambienteLight = new this.game.engine.AmbientLight(0x555555);
		this.scene.add(this.ambienteLight);

		this.light = new this.game.engine.DirectionalLight( 0xffffff, 0.75 );
		this.light.position.set(0, 0, 1);
		this.scene.add( this.light );
	}

	start()
	{
		this.init();

		// start scene
		this.is_started = true;
	}

	setCamera(camera)
	{
		this.camera = camera
		this.scene.add(camera);
	}

	render()
	{
		this.renderer.render( this.scene, this.camera );
	}

	update(deltaTime)
	{
		// Update controls
		this.stats.update();
	}

	updateCamera()
	{
		this.camera.updateProjectionMatrix();
	}

	draw()
	{
		this.entities.forEach(e => this.update(this.delta));
	}

	play()
	{
		this.animate();
		this.is_pause = false;

		if( this.sound )
			this.sound.play();
	}

	stop()
	{
		if( this.is_pause )
			return this.play();

		this.is_pause = true;

		if( this.sound )
			this.sound.stop();
	}

	animate()
	{
		this.delta += this.game.clock.getDelta();

		if( this.is_pause )
			return false;

		let interval = (1 / this.game.fps);
		let self = this;
		requestAnimationFrame( () => self.animate(self.delta) );

		//setTimeout( function()
		//{
		//}, 1000 / 60 ); // 30 fps

		if ( this.delta > interval )
		{
			// The draw or time dependent code are here
			this.update(this.delta);
			this.render();

			this.delta = this.delta % interval;
		}
	}

	setSize(width, height)
	{
		this.game.width = width;
		this.game.height = height;

		console.log("set size")

		if ( this.camera )
		{
			this.camera.aspect = this.game.width / this.game.height;
			this.camera.updateProjectionMatrix();
		}

		this.renderer.setSize( width, height );
	}

	resize()
	{
		this.setSize( window.innerWidth, window.innerHeight );
	}

	randomIntFromInterval(min, max) { // min and max included
		return Math.floor(Math.random() * (max - min + 1) + min)
	}

	clearAll()
	{
		while(this.scene.children.length > 0)
		{
			console.log(this.scene.children[0]);
			this.scene.remove(this.scene.children[0]);
		}

		while(this.camera.children.length > 0)
		{
			this.camera.remove(this.camera.children[0]);
		}

		if( this.sound )
			this.sound.stop();
	}

}