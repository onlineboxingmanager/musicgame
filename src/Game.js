import {IntroScene} from "./scenes/IntroScene.js";
import {FightScene} from "./scenes/FightScene.js";

export class Game
{
	constructor(engine)
	{
		this.engine = engine;
		this.fps = 60;

		this.player = null;

		this.scenes = [
			//new FightScene(this),
			//new IntroScene(this),
			new FightScene(this),
		];
		this.sceneid = 0;
		this.scene = this.scenes[0];

		this.clock = new this.engine.Clock();
		this.events = {};

		this.init();
		this.bindings();
		this.start();
	}

	init()
	{

	}

	bindings()
	{
		let self = this;

		document.onkeydown = function(e)
		{
			console.log(e.code);
			switch (e.code)
			{
				case "KeyD":
				case "ArrowRight":
					if( self.player )
						self.player.moveRight();

					break;

				case "KeyA":
				case "ArrowLeft":
					if( self.player )
						self.player.moveLeft();

					break;

				case "KeyW":
				case "ArrowUp":
				case "Space":
					if( self.player )
						self.player.moveUp();

					break;

				case "KeyS":
				case "ArrowDown":

					break;

				case "Escape":
					self.stop();
					break;

				case "Backspace":
					self.reset();
					break;

				case "Enter":
					self.doEnter();
					break;
			}
		}
	}

	setPixelRatio = function ( pixelRatio )
	{
		this.renderer.setPixelRatio( pixelRatio );
	};

	start()
	{
		this.scene.start();
		this.play();
	}

	play()
	{
		this.scene.play();
	}

	stop()
	{
		this.scene.stop();
	}

	nextScene()
	{
		this.stop();
		this.scene.clearAll();

		this.sceneid++;
		if( this.scenes[this.sceneid] )
		{
			this.scene = this.scenes[this.sceneid];
			this.start();
		}
	}

	reset()
	{
		this.stop();
		this.scene.clearAll();

		this.sceneid = 0;
		if( this.scenes[this.sceneid] )
		{
			this.scene = this.scenes[this.sceneid];
			this.start();
		}
	}

	doEnter()
	{
		this.scene.on_enter_command();
	}

}